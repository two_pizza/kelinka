<?php

$result = [];
$menu = $iiko->NomenclatureApi()->getMenu($organization['id']);

$mappedProducts = [];
foreach ($menu['products'] as $product ) {
    $mappedProducts[$product['id']] = $product;
}
$menu['products'] = $mappedProducts;

$mappedGroups = [];
foreach ($menu['groups'] as $group ) {
    $mappedGroups[$group['id']] = $group;
}
usort($mappedGroups, function ($a, $b) {
    return $a["order"] - $b["order"];
});
$menu['groups'] = $mappedGroups;

$result = array_merge($result, $menu);

$terminals = $iiko->DeliverySettingsApi()->getDeliveryTerminals($organization['id']);
$mappedResult = [];
foreach ($terminals['deliveryTerminals'] as &$terminal ) {
    $terminal['deliveryRestaurantName'] = preg_replace('/[0-9]+/', '', $terminal['deliveryRestaurantName']); # Убираем цифры из названия города (eg. Актау2)
    $mappedResult['deliveryTerminals'][$terminal['deliveryTerminalId']] = $terminal;
}
$terminals = $mappedResult;
$result = array_merge($result, $terminals);

$cities = $iiko->CitiesApi()->getCities($organization['id']);
$mappedResult = [];
foreach ($cities as $city ) {
    $mappedResult[$city['name']] = $city;
}
$cities = $mappedResult;
$result = array_merge($result, ['cities' => $cities]);