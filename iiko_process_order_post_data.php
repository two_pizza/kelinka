<?php

$postParams = $_POST;

$postParams['organization'] =  $organization['id'];
$postParams['order']['date'] = date('Y-m-d H:i:s');
$postParams['order']['phone'] = $postParams['customer']['phone'];
$fullSum = 0;
foreach ($postParams['order']['items'] as $item) {
    $fullSum += $item['sum'];
}
$postParams['order']['fullSum'] = $fullSum;
$postParams['order']['address'] = $postParams['address'];
return $postParams;