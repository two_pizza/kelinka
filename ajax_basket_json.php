<?php

session_start();
$basket = [];
if (isset($_SESSION['basket'])) {
    foreach ($_SESSION['basket'] as $id => $quantity) {
        $basket[] = ['id' => $id, 'delay' => true];
    }
}
$result = [
    'basket' => $basket,
    'compare' => []
];
echo json_encode($result);
die;