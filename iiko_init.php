<?php

use Iiko\Biz\Client as IikoClient;

require_once(__DIR__ . '/vendor/autoload.php');

$configPath = __DIR__.'/config/config.yml';
$yamlParser = new \Symfony\Component\Yaml\Parser();
$config = $yamlParser->parse(file_get_contents($configPath));

$iiko = new IikoClient([
    'user_id' => $config['iiko_api_user'],
    'user_secret' => $config['iiko_api_pass']
]);

$organization = $iiko->OrganizationsApi()->getList()[0];
