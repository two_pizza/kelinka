<?php

session_start();
$_SESSION["terminalId"] = $_REQUEST['region'];
if ($_SERVER['SCRIPT_FILENAME'] == str_replace(DIRECTORY_SEPARATOR,"/", __FILE__)) {
    $url = $_SERVER['HTTP_REFERER'];
    if(!$url) {
        $url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/';
    }
    header('Location: '.$url);
}
