<?php

require 'iiko_init.php';
require 'iiko_fetch.php';

$mode = $_REQUEST['mode'];
$devMode = $_REQUEST['mode'] == 'dev';

echo "<h3>Категории</h3>";
echo "<br>";
foreach ($result['groups'] as $group) {
    if ($devMode) {
        echo '<pre>';
        var_dump($group);
        echo '</pre>';
    } else {
        echo "{$group['id']}: # {$group['name']}<br>";
    }
}
echo "<br>";
echo "<h3>Продукты</h3>";
echo "<br>";

foreach ($result['products'] as $product) {
    if ($devMode) {
        echo '<pre>';
        var_dump($product);
        echo '</pre>';
    } else {
        echo "{$product['id']}: # {$product['name']}<br>";
    }
}
echo "<br>";
echo "<h3>Терминалы</h3>";
echo "<br>";
foreach ($result['deliveryTerminals'] as $terminal) {
    if ($devMode) {
        echo '<pre>';
        var_dump($terminal);
        echo '</pre>';
    } else {
        echo "{$terminal['deliveryTerminalId']}: # {$terminal['deliveryRestaurantName']}<br>";
    }
}
echo "<br>";
echo "<h3>Города</h3>";
echo "<br>";
foreach ($result['cities'] as $city) {
    if ($devMode) {
        echo '<pre>';
        var_dump($city);
        echo '</pre>';
    } else {
        echo "{$city['id']}: # {$city['name']}<br>";
    }
}
