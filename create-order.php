<?php

require 'iiko_init.php';

$postParams = require 'iiko_process_order_post_data.php';

$newOrder = $iiko->OrdersApi()->addOrder($postParams);
$order = $iiko->OrdersApi()->getInfo($organization['id'], $newOrder['orderId']);

session_start();
$_SESSION['basket'] = [];

header("Location: /order-success?orderId={$order['number']}&date={$order['createdTime']}");
exit(0);

