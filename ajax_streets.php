<?php

require 'iiko_init.php';

$cityId = $_GET['cityId'];
$cities = $iiko->CitiesApi()->getStreets($organization['id'], $cityId);
$result = [];
foreach ($cities as $city) {
    $result[] = [
        'id' => $city['id'],
        'text' => $city['name']
    ];
}
echo json_encode($result);
die;