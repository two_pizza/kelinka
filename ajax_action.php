<?php

if (isset($_POST['ajax'])) {
    session_start();
    if ($_POST['ajax']['action'] == 'basket.add') {
        $id = $_POST['ajax']['data']['id'];
        $quantity = $_POST['ajax']['data']['quantity'];
        if (isset($_SESSION['basket'][$id])) {
            $_SESSION['basket'][$id] += $quantity;
        } else {
            $_SESSION['basket'][$id] = $quantity;
        }
    } elseif ($_POST['ajax']['action'] == 'basket.remove') {
        $id = $_POST['ajax']['data']['id'];
        $quantity = $_POST['ajax']['data']['quantity'];
        if (isset($_SESSION['basket'][$id]) and $_SESSION['basket'][$id] > 1) {
            $_SESSION['basket'][$id] -= $quantity;
        } elseif(isset($_SESSION['basket'][$id])) {
            unset($_SESSION['basket'][$id]);
        }
    } elseif ($_POST['ajax']['action'] == 'basket.clear') {
        $id = $_POST['ajax']['data']['id'];
        unset($_SESSION['basket'][$id]);
    }
    array_filter($_SESSION['basket']);
    echo 'true';
}
die;
