<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

namespace utilsutils\utils\console\controllers;

use utilsutils\utils\Utils;

use Craft;
use yii\console\Controller;
use yii\helpers\Console;
use craft\elements\Entry;
use craft\elements\Asset;


class DefaultController extends Controller
{
    public function actionExport()
    {
        $baseUrl = 'https://reliablebanks.com/branches/{p}.json';
        $page_size = 1000;
        $count = 88633;
        $p = 1;
        while($count > ($p - 1)  * $page_size) {
            $entries = Entry::find()->anyStatus()->section('branches')->limit($page_size)->offset(($p - 1) * $page_size)->orderBy(['id' => SORT_ASC]);
            $out= [];
            foreach ($entries as $entry) {
                $banks = $entry->bank->asArray()->one();
                $locations = $entry->location->asArray()->all();
                $entryArray = $entries = Entry::find()->anyStatus()->section('branches')->id($entry->id)->asArray()->one();
                foreach (['field_hours', 'field_map'] as $field) {
                    $entryArray[$field] = json_decode($entryArray[$field], 1);
                }
                $entryArray['banks'] = $banks;
                $entryArray['locations'] = $locations;
                $out['results'][] = $entryArray;
            }
            $out['next'] = ($count > ($p) * $page_size) ? str_replace('{p}',intval($p + 1), $baseUrl)  : null;
            file_put_contents('/home/runcloud/webapps/finances/public/branches/'.$p.'.json', json_encode($out));
            $p++;
        }
    }

    public function actionExportTest()
    {
        $baseUrl = 'https://reliablebanks.com/branches/{p}.json';
        $page_size = 2;
        $count = 88633;
        $p = 1;
        while($count > ($p - 1)  * $page_size) {
            $entries = Entry::find()->anyStatus()->section('branches')->limit($page_size)->offset(($p - 1) * $page_size)->orderBy(['id' => SORT_ASC]);
            $out= [];
            foreach ($entries as $entry) {
                $banks = $entry->bank->asArray()->one();
                $locations = $entry->location->asArray()->all();
                $entryArray = $entries = Entry::find()->anyStatus()->section('branches')->id($entry->id)->asArray()->one();
                foreach (['field_hours', 'field_map'] as $field) {
                    $entryArray[$field] = json_decode($entryArray[$field], 1);
                }
                $entryArray['banks'] = $banks;
                $entryArray['locations'] = $locations;
                $out['results'][] = $entryArray;
            }
            $out['next'] =null;
            file_put_contents('/home/runcloud/webapps/finances/public/branches/'.$p.'_test.json', json_encode($out));
            $p++;
            die;
        }
    }

    public function actionExportSolutions()
    {
        $string = file_get_contents("/home/runcloud/webapps/finances/solutions.json");
        $json=json_decode($string,true);

        foreach ($json as &$result){
            foreach ($result as $key => &$property) {
                if ($key == 'chexsystemsBanks') {
                    foreach ($property as $key =>&$block) {
                        foreach ($block as $key => &$field) {
                            if ($key == 'fields') {
                                foreach ($field as $key => &$ids) {
                                    if ($key == 'institution') {
                                        foreach ($ids as &$id) {
                                            $entry = Entry::find()->anyStatus()->id($id)->one();
                                            $id = $entry->slug;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $fp = fopen('/home/runcloud/webapps/finances/public/solutions_result.json', 'w');
        fwrite($fp, json_encode($json));
        fclose($fp);
    }
    public function actionExportInstitutions()
    {
        $string = file_get_contents("/home/runcloud/webapps/finances/institutions.json");
        $json=json_decode($string,true);

        foreach ($json as &$result){
            foreach ($result as $key => &$property) {
                if ($key == 'institutionLogo') {
                    foreach ($property as &$id) {
                        $entry = Asset::find()->anyStatus()->id($id)->one();
                        $id = 'https://reliablebanks.com'.$entry->url;
                        var_dump($id);
                    }
                }
            }
        }

        $fp = fopen('/home/runcloud/webapps/finances/public/institutions_result.json', 'w');
        fwrite($fp, json_encode($json));
        fclose($fp);
    }
}
