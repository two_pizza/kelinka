<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      mailto:borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

/**
 * Utils en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('utils', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Dmitry Borovkov
 * @package   Utils
 * @since     1
 */
return [
    'Utils plugin loaded' => 'Utils plugin loaded',
];
