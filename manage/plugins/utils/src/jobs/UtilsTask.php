<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

namespace utilsutils\utils\jobs;

use utilsutils\utils\Utils;

use Craft;
use craft\queue\BaseJob;
use craft\elements\Asset;
use craft\models\VolumeFolder;
use craft\helpers\ElementHelper;

/**
 * UtilsTask job
 *
 * Jobs are run in separate process via a Queue of pending jobs. This allows
 * you to spin lengthy processing off into a separate PHP process that does not
 * block the main process.
 *
 * You can use it like this:
 *
 * use utilsutils\utils\jobs\UtilsTask as UtilsTaskJob;
 *
 * $queue = Craft::$app->getQueue();
 * $jobId = $queue->push(new UtilsTaskJob([
 *     'description' => Craft::t('utils', 'This overrides the default description'),
 *     'someAttribute' => 'someValue',
 * ]));
 *
 * The key/value pairs that you pass in to the job will set the public properties
 * for that object. Thus whatever you set 'someAttribute' to will cause the
 * public property $someAttribute to be set in the job.
 *
 * Passing in 'description' is optional, and only if you want to override the default
 * description.
 *
 * More info: https://github.com/yiisoft/yii2-queue
 *
 * @author    Dmitry Borovkov
 * @package   Utils
 * @since     1
 */
class UtilsTask extends BaseJob
{
    public $entry = null;
    public $field = null;
    public $volumeId = null;
    public $parentId = null;

    // Public Methods
    // =========================================================================

    /**
     * When the Queue is ready to run your job, it will call this method.
     * You don't need any steps or any other special logic handling, just do the
     * jobs that needs to be done here.
     *
     * More info: https://github.com/yiisoft/yii2-queue
     */
    public function execute($queue)
    {
       
    }

    // Protected Methods
    // =========================================================================

    /**
     * Returns a default description for [[getDescription()]], if [[description]] isn’t set.
     *
     * @return string The default task description
     */
    protected function defaultDescription(): string
    {
        return Craft::t('utils', 'UtilsTask');
    }
}
