/**
 * Utils plugin for Craft CMS
 *
 * Utils JS
 *
 * @author    Dmitry Borovkov
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 * @link      mailto:borovkov.dmitry@gmail.com
 * @package   Utils
 * @since     1
 */
