/**
 * Utils plugin for Craft CMS
 *
 * UtilsUtility Utility JS
 *
 * @author    Dmitry Borovkov
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 * @link      mailto:borovkov.dmitry@gmail.com
 * @package   Utils
 * @since     1
 */
