<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      mailto:borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

namespace utilsutils\utils\utilities;

use utilsutils\utils\Utils;
use utilsutils\utils\assetbundles\utilsutilityutility\UtilsUtilityUtilityAsset;

use Craft;
use craft\base\Utility;

/**
 * Utils Utility
 *
 * Utility is the base class for classes representing Control Panel utilities.
 *
 * https://craftcms.com/docs/plugins/utilities
 *
 * @author    Dmitry Borovkov
 * @package   Utils
 * @since     1
 */
class UtilsUtility extends Utility
{
    // Static
    // =========================================================================

    /**
     * Returns the display name of this utility.
     *
     * @return string The display name of this utility.
     */
    public static function displayName(): string
    {
        return Craft::t('utils', 'UtilsUtility');
    }

    /**
     * Returns the utility’s unique identifier.
     *
     * The ID should be in `kebab-case`, as it will be visible in the URL (`admin/utilities/the-handle`).
     *
     * @return string
     */
    public static function id(): string
    {
        return 'utils-utils-utility';
    }

    /**
     * Returns the path to the utility's SVG icon.
     *
     * @return string|null The path to the utility SVG icon
     */
    public static function iconPath()
    {
        return Craft::getAlias("@utilsutils/utils/assetbundles/utilsutilityutility/dist/img/UtilsUtility-icon.svg");
    }

    /**
     * Returns the number that should be shown in the utility’s nav item badge.
     *
     * If `0` is returned, no badge will be shown
     *
     * @return int
     */
    public static function badgeCount(): int
    {
        return 0;
    }

    /**
     * Returns the utility's content HTML.
     *
     * @return string
     */
    public static function contentHtml(): string
    {
        Craft::$app->getView()->registerAssetBundle(UtilsUtilityUtilityAsset::class);

        $someVar = 'Have a nice day!';
        return Craft::$app->getView()->renderTemplate(
            'utils/_components/utilities/UtilsUtility_content',
            [
                'someVar' => $someVar
            ]
        );
    }
}
