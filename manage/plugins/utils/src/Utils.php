<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      mailto:borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

namespace utilsutils\utils;

use utilsutils\utils\services\UtilsService as UtilsServiceService;
use utilsutils\utils\variables\UtilsVariable;
use utilsutils\utils\twigextensions\UtilsTwigExtension;
use utilsutils\utils\models\Settings;
use utilsutils\utils\fields\UtilsField as UtilsFieldField;
use utilsutils\utils\utilities\UtilsUtility as UtilsUtilityUtility;
use utilsutils\utils\widgets\UtilsWidget as UtilsWidgetWidget;
use utilsutils\utils\behaviors\UtilsBehavior;
use craft\elements\db\AssetQuery;
use \Symfony\Component\Yaml\Parser;
use \Symfony\Component\Yaml\Yaml;
use yii\helpers\ArrayHelper;
use craft\elements\db\EntryQuery;
use verbb\supertable\elements\db\SuperTableBlockQuery;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\console\Application as ConsoleApplication;
use craft\web\UrlManager;
use craft\services\Elements;
use craft\services\Fields;
use craft\services\Utilities;
use craft\web\twig\variables\CraftVariable;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use craft\elements\Entry;
use craft\events\ModelEvent;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://docs.craftcms.com/v3/extend/
 *
 * @author    Dmitry Borovkov
 * @package   Utils
 * @since     1
 *
 * @property  UtilsServiceService $utilsService
 * @property  Settings $settings
 * @method    Settings getSettings()
 */
class Utils extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * Utils::$plugin
     *
     * @var Utils
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public $schemaVersion = '1';

    /**
     * Set to `true` if the plugin should have a settings view in the control panel.
     *
     * @var bool
     */
    public $hasCpSettings = true;

    /**
     * Set to `true` if the plugin should have its own section (main nav item) in the control panel.
     *
     * @var bool
     */
    public $hasCpSection = true;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * Utils::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Add in our Twig extensions
        Craft::$app->view->registerTwigExtension(new UtilsTwigExtension());

        // Add in our console commands
        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'utilsutils\utils\console\controllers';
        }

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'utils/default';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'utils/default/do-something';
            }
        );

        // Register our elements
        Event::on(
            Elements::class,
            Elements::EVENT_REGISTER_ELEMENT_TYPES,
            function (RegisterComponentTypesEvent $event) {
            }
        );

        // Register our fields
        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = UtilsFieldField::class;
            }
        );

        // Register our utilities
/*        Event::on(
            Utilities::class,
            Utilities::EVENT_REGISTER_UTILITY_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = UtilsUtilityUtility::class;
            }
        );*/

        // Register our widgets
        Event::on(
            Dashboard::class,
            Dashboard::EVENT_REGISTER_WIDGET_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = UtilsWidgetWidget::class;
            }
        );

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('utils', UtilsVariable::class);
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function(Event $event) {
                $event->sender->attachBehaviors([
                    UtilsBehavior::class,
                ]);
            });

        Event::on(
            Entry::class,
            Entry::EVENT_AFTER_SAVE,
            function(ModelEvent $event) {
                $configDirPath = '../../config';
                $settings = Entry::find()->section('settings')->one();
                $fields = $settings->fieldValues;
                foreach ($fields as $key => &$field) {
                    if (is_a($field, AssetQuery::class)) {
                        if($entry = $field->one()) {
                            $field = $entry->url;
                        } else {
                            $field = null;
                        }
                    }
                    if ($key == 'default_terminal_id') {
                        if($entry = $field->one()) {
                            $field = $entry->iiko_id;
                        } else {
                            $field = null;
                        }
                    }
                }

                $configTerminals = [];
                $terminals = Entry::find()->section('terminal')->all();
                foreach ($terminals as $terminal) {
                    $configTerminals[$terminal->iiko_id] = $terminal->fieldValues;
                }
                foreach ($configTerminals as $key => &$configTerminal) {
                    foreach ($configTerminal as $fieldKey =>&$field ) {
                        if ($fieldKey == 'social') {
                            $values = $field[0];
                            foreach ($values as $valueKey => $value) {
                                if (strpos($valueKey, 'col') !== false) {
                                    unset($values[$valueKey]);
                                }
                            }
                            $field = $values;
                        }

                        if (is_a($field, SuperTableBlockQuery::class)) {
                            $rows = $field->all();
                            foreach ($rows as &$row) {
                                $row = $row->fieldValues;
                                foreach ($row as &$block) {
                                    if (is_a($block, AssetQuery::class)) {
                                        if($entry = $block->one()) {
                                            $block = $entry->url;
                                        } else {
                                            $block = null;
                                        }
                                    }
                                }
                            }
                            $field = $rows;
                        }

                        if (is_a($field, AssetQuery::class)) {
                            if($entry = $field->one()) {
                                $field = $entry->url;
                            } else {
                                $field = null;
                            }
                        }
                    }
                }
                $yamlParser = new Parser();
                $configLocalPath = $configDirPath.'/config_local.yml';
                $configLocal = $yamlParser->parse(file_get_contents($configLocalPath));
                $config = ArrayHelper::merge($configLocal, $fields);
                $config = ArrayHelper::merge($config, ['terminals' => ['items' => $configTerminals]]);
                $yaml = Yaml::dump($config);
                file_put_contents($configDirPath.'/config.yml', $yaml);
            }
        );

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'utils',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates and returns the model used to store the plugin’s settings.
     *
     * @return \craft\base\Model|null
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return string The rendered settings HTML
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'utils/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }
}
