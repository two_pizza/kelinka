<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      mailto:borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

namespace Helper;

use Codeception\Module;

/**
 * Class Functional
 *
 * Here you can define custom actions.
 * All public methods declared in helper class will be available in $I
 */
class Functional extends Module
{

}
