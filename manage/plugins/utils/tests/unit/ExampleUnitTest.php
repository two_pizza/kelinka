<?php
/**
 * Utils plugin for Craft CMS 3.x
 *
 * Utils
 *
 * @link      mailto:borovkov.dmitry@gmail.com
 * @copyright Copyright (c) 2020 Dmitry Borovkov
 */

namespace utilsutils\utilstests\unit;

use Codeception\Test\Unit;
use UnitTester;
use Craft;
use utilsutils\utils\Utils;

/**
 * ExampleUnitTest
 *
 *
 * @author    Dmitry Borovkov
 * @package   Utils
 * @since     1
 */
class ExampleUnitTest extends Unit
{
    // Properties
    // =========================================================================

    /**
     * @var UnitTester
     */
    protected $tester;

    // Public methods
    // =========================================================================

    // Tests
    // =========================================================================

    /**
     *
     */
    public function testPluginInstance()
    {
        $this->assertInstanceOf(
            Utils::class,
            Utils::$plugin
        );
    }

    /**
     *
     */
    public function testCraftEdition()
    {
        Craft::$app->setEdition(Craft::Pro);

        $this->assertSame(
            Craft::Pro,
            Craft::$app->getEdition()
        );
    }
}
