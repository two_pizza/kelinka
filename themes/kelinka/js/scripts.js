(function ($) {
    $(document).on('ready', function () {
        $('a[href^="#"]').on('click', function () {
            var html = $('html');
            var offset = 75;
            if (html.hasClass('bx-android') || html.hasClass('bx-ios')) {
                offset = 20;
            }
            html.removeAttr("style");
            $(document).find('.menu-panel-button[data-action="menu.close"]').click();
            if ($($(this).attr('href')).length) {
                $('html, body').animate({
                    scrollTop: $($(this).attr('href')).offset().top - offset
                }, 200);
            }
            return false;
        });


        $('.bx-soa-pp-company').on('click', function () {
            $('.bx-soa-pp-company').removeClass('bx-selected');
            $( ".bx-soa-pp-company input" ).each(function( index ) {
                $(this).prop("checked", false);
            });
            $(".check-order-btn").hide();
            $(".delivery-price").hide();

            $(this).addClass('bx-selected');
            $(this).find('input').prop("checked", true);
            var delivery_type = $(this).data('delivery-type');
            $(".check-order-"+delivery_type+"-btn").show();
            $(".delivery-price-"+delivery_type).show();

        });

        $('.check-order-btn').on('click', function () {
            var form_selector = '#bx-soa-order-form';
            var items_selector = '#order_items';
            $(form_selector + ' ' + items_selector).val(jQuery.makeArray($(form_selector + ' ' + items_selector).data('json')));
            var form = $(form_selector);
            var form_valid = form[0].checkValidity();
            if (form_valid) {
                $.ajax({
                    url: '/check-order.php',
                    type: 'post',
                    data: form.serialize(),
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        if (data.problem == null) {
                            //if($('[name="order[isSelfService]"]:checked').val() == 'true') {
                                $('#bx-soa-order-form').submit();
                            //}
                            /*else{
                                var delivery_price = 0;
                                if (data.deliveryServiceProductInfo !== null && typeof data.deliveryServiceProductInfo.productSum !== 'undefined') {
                                    delivery_price = data.deliveryServiceProductInfo.productSum;
                                }
                                $('.delivery-price-final span').html(delivery_price);
                                $('.cart-total-value').html(parseInt($('.cart-subtotal-value').html()) + parseInt(delivery_price));
                                $('.delivery-price').hide();
                                $('.delivery-price-final').show();
                                $('.check-order-btn').hide();
                                $('.create-order-btn').show();
                            }*/
                        } else {
                            alert(data.problem);
                        }
                    }
                });
                return false;
            }
        });

        /*$('#bx-soa-order-form #soa-property-address-street, #bx-soa-order-form #soa-property-address-home').on('input keyup change', function () {
            if($('[name="order[isSelfService]"]:checked').val() == 'false') {
                $('.delivery-price-courier').show();
                $('.check-order-courier-btn').show();
                $('.delivery-price-final').hide();
                $('.create-order-btn').hide();
                $('.cart-total-value').html(parseInt($('.cart-subtotal-value').html()));
            }
        });*/

        $('#soa-property-customer-phone').mask('+7 (000) 000 00 00', {clearIfNotMatch: true, placeholder: "+7 (___) ___ __ __"});

        $('.widget-item-purchase-button-add').on('click', function () {
            var el = $(this);
            setTimeout(function(){
                $.get('/cart-ajax', {'template': 'cart-desktop'}, function (result) {
                    $('#i-4-intec-universe-sale-basket-small-template-2-gnX3eXc-Vj5u').replaceWith(result);
                });
                $.get('/cart-ajax', {'template': 'cart-mobile'}, function (result) {
                    $('#i-6-intec-universe-sale-basket-small-panel-1-rV0l0WXgSyb7').replaceWith(result);
                });
                $.get('/cart-ajax', {'template': 'notification', 'id': el.data('basket-id')}, function (result) {
                    $('.c-sale-basket-small-notifications-1 .sale-basket-small-products').prepend(result);
                    setTimeout(function(){
                        $(document).find('.c-sale-basket-small-notifications-1 .sale-basket-small-products').empty();
                    }, 2000);
                });
                $.get('/cart-ajax', {'template': 'cart-menu'}, function (result) {
                    $('.menu-basket-panel-button-wrap').replaceWith(result);
                });
            }, 300);
        });

        $(document).on('click','.sale-basket-small-product-btn-close',  function () {
            $(document).find('.c-sale-basket-small-notifications-1 .sale-basket-small-products').empty();
        });

        $(document).on('click','[data-action="close"]',  function () {
            $(document).find('.sale-basket-small-overlay').click();
        });

        $(document).on('click','[data-action="product.remove"]',  function () {
            $(this).parents('.sale-basket-small-product').remove();
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.clear', 'data' :{'id': $(this).parents('.sale-basket-small-product').data('id')}}}, function( data ) {
                $.get('/cart-ajax', {'template': 'cart-desktop'}, function (result) {
                    $('#i-4-intec-universe-sale-basket-small-template-2-gnX3eXc-Vj5u').replaceWith(result);
                    $('.sale-basket-small-overlay').css("width",  "100%").css("height", "100%").css("opacity", "1");
                    $('.sale-basket-small-switch').addClass('active').data('active', 'true');
                    $('.sale-basket-small-tab').show();
                });
            });
        });
        $(document).on('click','.sale-basket-small-content [data-action="increment"]',  function () {
            $(this).parents('.sale-basket-small-product').find('.sale-basket-small-product-quantity-value').val(function(i, oldval) {
                return ++oldval;
            });
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.add', 'data' :{'id': $(this).parents('.sale-basket-small-product').data('id'), 'quantity': 1}}}, function( data ) {
                $.get('/cart-ajax', {'template': 'cart-desktop'}, function (result) {
                    $('#i-4-intec-universe-sale-basket-small-template-2-gnX3eXc-Vj5u').replaceWith(result);
                    $('.sale-basket-small-overlay').css("width",  "100%").css("height", "100%").css("opacity", "1");
                    $('.sale-basket-small-switch').addClass('active').data('active', 'true');
                    $('.sale-basket-small-tab').show();
                });
            });
        });

        $(document).on('click','.sale-basket-small-content [data-action="decrement"]',  function () {
            $(this).parents('.sale-basket-small-product').find('.sale-basket-small-product-quantity-value').val(function(i, oldval) {
                return --oldval;
            });
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.remove', 'data' :{'id': $(this).parents('.sale-basket-small-product').data('id'), 'quantity': 1}}}, function( data ) {
                $.get('/cart-ajax', {'template': 'cart-desktop'}, function (result) {
                    $('#i-4-intec-universe-sale-basket-small-template-2-gnX3eXc-Vj5u').replaceWith(result);
                    $('.sale-basket-small-overlay').css("width",  "100%").css("height", "100%").css("opacity", "1");
                    $('.sale-basket-small-switch').addClass('active').data('active', 'true');
                    $('.sale-basket-small-tab').show();
                });
            });
        });

        $(document).on('click','#bx-soa-total [data-entity="basket-item-quantity-plus"]',  function () {
            var container = $(this).parent().parent().parent().parent().parent().parent();
            container.find('.intec-ui-part-input').val(function(i, oldval) {
                return ++oldval;
            });
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.add', 'data' :{'id': container.data('id'), 'quantity': 1}}}, function( data ) {
                $.get('/order-cart', {}, function (result) {
                    $('#bx-soa-basket').replaceWith(result);
                });
                $.get('/order-form-fields', {}, function (result) {
                    $('#order-form-fields').replaceWith(result);
                });
                $.get('/order-total', {}, function (result) {
                    $('.cart-total-value').html(result);
                    $('.cart-subtotal-value').html(result);
                });
            });
        });

        $(document).on('click','#bx-soa-total [data-entity="basket-item-quantity-minus"]',  function () {
            var container = $(this).parent().parent().parent().parent().parent().parent();
            container.find('.intec-ui-part-input').val(function(i, oldval) {
                return --oldval;
            });
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.remove', 'data' :{'id': container.data('id'), 'quantity': 1}}}, function( data ) {
                $.get('/order-cart', {}, function (result) {
                    $('#bx-soa-basket').replaceWith(result);
                });
                $.get('/order-form-fields', {}, function (result) {
                    $('#order-form-fields').replaceWith(result);
                });
                $.get('/order-total', {}, function (result) {
                    $('.cart-total-value').html(result);
                    $('.cart-subtotal-value').html(result);
                });
            });
        });

        $(document).on('click','.basket-items [data-entity="basket-item-delete"]',  function () {
            $(this).parents('.basket-item').remove();
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.clear', 'data' :{'id': $(this).parents('.basket-item').data('id')}}}, function( data ) {
                $.get('/cart-ajax', {'template': 'cart-page'}, function (result) {
                    $('.intec-template-page').replaceWith(result);
                });
            });
        });
        $(document).on('click','.basket-items [data-entity="basket-item-quantity-plus"]',  function () {
            $(this).parents('.basket-item').find('.intec-ui-part-input').val(function(i, oldval) {
                return ++oldval;
            });
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.add', 'data' :{'id': $(this).parents('.basket-item').data('id'), 'quantity': 1}}}, function( data ) {
                $.get('/cart-ajax', {'template': 'cart-page'}, function (result) {
                    $('.intec-template-page').replaceWith(result);
                });
            });
        });

        $(document).on('click','.basket-items [data-entity="basket-item-quantity-minus"]',  function () {
            $(this).parents('.basket-item').find('.intec-ui-part-input').val(function(i, oldval) {
                return --oldval;
            });
            $.post( "/ajax_action.php", {'ajax': {'action': 'basket.remove', 'data' :{'id': $(this).parents('.basket-item').data('id'), 'quantity': 1}}}, function( data ) {
                $.get('/cart-ajax', {'template': 'cart-page'}, function (result) {
                    $('.intec-template-page').replaceWith(result);
                });
            });
        });



        $('input[name="address[city]"]').on('click', function () {
            $('.regions-select-region').click()
        });

        if ($('#soa-property-address-street').length) {
            var cityId = $('#soa-property-address-street').data('city-id');

            $.get('/ajax_streets.php?cityId=' + cityId, function (data) {
                var options = $.parseJSON(data);
                $.each(options, function (i, item) {
                    $('#soa-property-address-street')
                        .append($('<option>', {value: item.text})
                            .text(item.text).attr('data-id', item.id));
                });
                $('#soa-property-address-street').selectize({
                    create: true,
                    placeholder: '',
                    onChange: function (value) {
                        var option = $('#soa-property-address-street option[value="' + value + '"]');
                        var id = '';
                        $.each(options, function (i, item) {
                            if (item.text == value) {
                                id = item.id;
                            }
                        });
                        $('#soa-property-address-streetId').val(id);
                    },
                    render: {
                        option_create: function (data, escape) {
                            return '<div class="create"> ' + escape(data.input) + '</div>';
                        }
                    },
                });
            })
        }

        $(document).on('click','.c-catalog-element-quick-view-2 [data-action="decrement"]',  function () {
            $(this).parent().find('input').val(function(i, oldval) {
                var val = --oldval;
                if (val < 1) {
                    val = 1;
                }
                $('.catalog-element-purchase-button-add').attr('data-basket-quantity', val);

                var quantity = val;
                var price = parseInt($('.catalog-element-purchase-button-add').attr('data-basket-price'));
                $('.catalog-element-offers-property-values').each(function( index ) {
                    if ($(this).find('.catalog-element-offers-property-value[data-state="selected"]').length) {
                        price += parseInt($(this).find('.catalog-element-offers-property-value[data-state="selected"]').data('price'));
                    }
                });
                var amount = quantity * price;
                $(this).parent().parent().parent().parent().parent().parent().parent().parent().find('.catalog-element-price-discount, .catalog-element-price-base').html(amount);

                return val;
            });
        });

        $(document).on('click','.c-catalog-element-quick-view-2 [data-action="increment"]',  function () {
            $(this).parent().find('input').val(function(i, oldval) {
                var val = ++oldval;
                $('.catalog-element-purchase-button-add').attr('data-basket-quantity', val);

                var quantity = val;
                var price = parseInt($('.catalog-element-purchase-button-add').attr('data-basket-price'));

                $('.catalog-element-offers-property-values').each(function( index ) {
                    if ($(this).find('.catalog-element-offers-property-value[data-state="selected"]').length) {
                        price += parseInt($(this).find('.catalog-element-offers-property-value[data-state="selected"]').data('price'));
                    }
                });
                var amount = quantity * price;
                $(this).parent().parent().parent().parent().parent().parent().parent().parent().find('.catalog-element-price-discount, .catalog-element-price-base').html(amount);

                return val;
            });
        });

        $(document).on('click','.catalog-element-offers-property-value',  function () {
            $(this).parent().parent().removeClass('has-error');
            $(this).parent().find('.catalog-element-offers-property-value').attr('data-state', 'enabled');
            $(this).attr('data-state', 'selected');
            $(this).parent().parent().parent().parent().parent().parent().parent().parent().find('.catalog-element-price-discount, .catalog-element-price-base').removeClass('exact')

            var quantity = $('.c-catalog-element-quick-view-2 .intec-ui-part-input').val();
            var id = $(this).parent().parent().parent().parent().find('.widget-item-purchase-button-add, .catalog-element-purchase-button-add').attr('data-base-basket-id');
            var price = parseInt($('.catalog-element-purchase-button-add').attr('data-basket-price'));
            var validated = true;
            $('.catalog-element-offers-property-values').each(function( index ) {
                id += '_' +$(this).find('.catalog-element-offers-property-value[data-state="selected"]').data('id')
                if ($(this).find('.catalog-element-offers-property-value[data-state="selected"]').length){
                    price += parseInt($(this).find('.catalog-element-offers-property-value[data-state="selected"]').data('price'));
                } else {
                    validated = false;
                }
            });
            var amount = quantity * price;
            $(this).parent().parent().parent().parent().parent().parent().parent().parent().find('.catalog-element-price-discount, .catalog-element-price-base').html(amount);

            if (validated) {
                $(this).parent().parent().parent().parent().parent().parent().parent().parent().find('.catalog-element-price-discount, .catalog-element-price-base').addClass('exact')
                $('.c-catalog-element-quick-view-2 .catalog-element-purchase-button-add').removeAttr("disabled")
                $(this).parent().parent().parent().parent().find('.widget-item-purchase-button-add, .catalog-element-purchase-button-add').attr('data-basket-id', id);
            }
        });

        $(document).on('click','.c-catalog-element-quick-view-2 .catalog-element-purchase-button-add',  function () {
            if ($(this).attr('disabled') == 'disabled') {
                $('.catalog-element-offers-property').each(function( index ) {
                    if (!$(this).find('.catalog-element-offers-property-value[data-state="selected"]').length){
                        $(this).addClass('has-error');
                    }
                });
                return;
            }
            var el = $(this);
            setTimeout(function(){
                $.get('/cart-ajax', {'template': 'cart-desktop'}, function (result) {
                    $('#i-4-intec-universe-sale-basket-small-template-2-gnX3eXc-Vj5u').replaceWith(result);
                });
                $.get('/cart-ajax', {'template': 'cart-mobile'}, function (result) {
                    $('#i-6-intec-universe-sale-basket-small-panel-1-rV0l0WXgSyb7').replaceWith(result);
                });
                $.get('/cart-ajax', {'template': 'notification', 'id': el.data('basket-id')}, function (result) {
                    $('.c-sale-basket-small-notifications-1 .sale-basket-small-products').prepend(result);
                    setTimeout(function(){
                        $(document).find('.c-sale-basket-small-notifications-1 .sale-basket-small-products').empty();
                    }, 2000);
                });
                $.get('/cart-ajax', {'template': 'cart-menu'}, function (result) {
                    $('.menu-basket-panel-button-wrap').replaceWith(result);
                });
                el.hide();
                el.next().css( "display", "block" );
            }, 300);
        });
    });

})(jQuery)