<?php

$filename = 'iiko_api_data.json';
$timediff = time()  - filectime ($filename);
if (file_exists($filename) and $timediff < 8 * 60 *60){
    $string = file_get_contents($filename);
    $result = json_decode($string,true);
} else {
    require 'iiko_init.php';
    require 'iiko_fetch.php';
    $fp = fopen($filename, 'w');
    fwrite($fp, json_encode($result));
    fclose($fp);
}
if ($_SERVER['SCRIPT_FILENAME'] == str_replace(DIRECTORY_SEPARATOR,"/", __FILE__)) {
    $json = json_encode($result);
    echo $json;
    die;
} else {
    return $result;
}
