<?php

class RequestPlugin extends AbstractPicoPlugin
{
    const API_VERSION = 2;

    protected $enabled = true;

    public function onTwigRegistered(Twig_Environment &$twig)
    {
        $twig->addGlobal('request', $_REQUEST);
    }
}