<?php

class IikoPlugin extends AbstractPicoPlugin
{
    const API_VERSION = 2;

    protected $enabled = true;

    public function onTwigRegistered(Twig_Environment &$twig)
    {
        $result = require (__DIR__.'/../iiko_api_data.php');
        $twig->addGlobal('iiko', $result);
    }
}