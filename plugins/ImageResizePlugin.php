<?php

class ImageResizePlugin extends AbstractPicoPlugin
{
    const API_VERSION = 2;

    protected $enabled = true;

    public function onTwigRegistered(Twig_Environment &$twig)
    {
        $function = new \Twig\TwigFunction('img_resize', function ($id, $url, $size) {
            $dir = 'uploads/'.$id;
            $path = $dir.'/'.$size.'.jpg';
            if (!file_exists($path)) {
                if (!is_dir('uploads/'.$id)) {
                    mkdir($dir);
                }
                $configPath = 'config/config.yml';
                $yamlParser = new \Symfony\Component\Yaml\Parser();
                $config = $yamlParser->parse(file_get_contents($configPath));
                $filename = basename($url);
                file_put_contents($filename, file_get_contents($url));
                foreach ($config['img_sizes'] as $size_name => $img_size) {
                    $dst = $this->resize_image($filename, $img_size['width'], null);
                    imagejpeg($dst, $dir.'/'.$size_name.'.jpg', 90);
                    $dst = $this->resize_image($filename, $img_size['width'] *2 , null);
                    imagejpeg($dst, $dir.'/'.$size_name.'@2x.jpg', 90);
                }
                @unlink($filename);
            }
            return $path;
        });
        $twig->addFunction($function);
    }

    public function resize_image($file, $w, $h) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        $newheight = $w/$r;
        $newwidth = $w;
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
}