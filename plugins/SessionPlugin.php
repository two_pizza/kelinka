<?php

class SessionPlugin extends AbstractPicoPlugin
{
    const API_VERSION = 2;

    protected $enabled = true;

    public function onTwigRegistered(Twig_Environment &$twig)
    {
        session_start();
        $twig->addGlobal('session', $_SESSION);
    }
}